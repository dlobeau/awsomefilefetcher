//
//  DataFromSecureSiteFetchingTest.swift
//  AwsomeDataFetcherTests
//
//  Created by Didier Lobeau on 22/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest

class DataFromSecureSiteFetchingTest: XCTestCase
{
    
    var fetcher:RemoteDataFetcher?
    var testEnvironment:TestEnvironment?
      
    let PathDb:String = "https://marlove.net/e/mock/v1/items"
    let authorizationKey = "6a3d841041274cb7461bc6a62b62a1bd"
    var cerFile:LocalFile?
    var wrongCerFile:LocalFile?
    var nonExistingCerFile:LocalFile?

    override func setUp()
    {
        testEnvironment = TestEnvironment.init()
        
        let CurrenBundle = testEnvironment!.bundle
        cerFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:"marlove_connection_certificate.crt")
       
        wrongCerFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:"marlove_wrong_certificate.perm")
        nonExistingCerFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:"lateteatoto.perm")
        fetcher = RemoteDataFetcherAlamo.init(WithCertificationFile: cerFile!, WithKey: authorizationKey)
        
    }

    override func tearDown()
    {
        
    }

    func testDataFromSecureSiteFetchingTest_whenFetchingData_FetchedDataSizeToReference()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        fetcher!.fetch(WithURL: PathDb,WithDelegate: currentDelegate!)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Stream = currentDelegate!.fetchedData as! Data
         
        XCTAssertTrue(Stream.count == 33268)
    }
    
    func testDataFromSecureSiteFetchingTest_whenFetchingImageWithRange_FetchedDataShouldBeInRange()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter.init(WithLimitLow: "5e4eb391bf530", WithLimitHigh: "5e4eb391d491d")
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Stream = currentDelegate!.fetchedData as! Data
          
        XCTAssertTrue(Stream.count == 9982)
    }
    
    func testDataFromSecureSiteFetchingTest_whenFetchingNext10Items_FetchedDataShouldBeInRange()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter(( "", "5e4eb3918836a"))
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Stream = currentDelegate!.fetchedData as! Data
          
        XCTAssertEqual(Stream.count ,32075)
    }
    
    func testDataFromSecureSiteFetchingTest_whenParametersAreOk_ErrorMustbeNil()
    {
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter(( "", "5e4eb3918836a"))
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Error = currentDelegate!.errorReported
       
        XCTAssertNil(Error)
    }
    
    func testDataFromSecureSiteFetchingTest_whenKeyIsWrong_ErrorMustNotBeNil()
    {
        fetcher = RemoteDataFetcherAlamo.init(WithCertificationFile: cerFile!, WithKey: "toto")
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter(( "", "5e4eb3918836a"))
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Error = currentDelegate!.errorReported
        print(Error!.localizedDescription)
        XCTAssertNotNil(Error)
    }
    
    func testDataFromSecureSiteFetchingTest_whenCertificateIsWrong_ErrorMustNotBeNil()
    {
        fetcher = RemoteDataFetcherAlamo.init(WithCertificationFile: wrongCerFile!, WithKey: authorizationKey)
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter(( "", "5e4eb3918836a"))
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Error = currentDelegate!.errorReported
        XCTAssertNotNil(Error)
    }
    
    func testDataFromSecureSiteFetchingTest_whenCertificateFileDontExist_ErrorMustNotBeNil()
    {
        fetcher = RemoteDataFetcherAlamo.init(WithCertificationFile: nonExistingCerFile!, WithKey: authorizationKey)
        let expectation = self.expectation(description: "fetching")
        let currentDelegate = testEnvironment?.createFetcherDelegate(WithExpectation: expectation)
        
        let Limits = LimitRequestParameter(( "", "5e4eb3918836a"))
        
        fetcher!.fetch(WithURL: PathDb, WithDelegate: currentDelegate!,WithLimits:Limits)
           
        let result = XCTWaiter.wait(for: [expectation], timeout: 10.0, enforceOrder: true)
        XCTAssertTrue(result == .completed)
        
        
        let Error = currentDelegate!.errorReported
        XCTAssertNotNil(Error)
    }

}
