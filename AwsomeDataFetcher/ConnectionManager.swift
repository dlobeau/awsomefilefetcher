//
//  ConnectionManager.swift
//  AwsomeDataFetcher
//
//  Created by Didier Lobeau on 23/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import Alamofire


class SSLPinningManager: SessionDelegate
{
    var sessionManager: Session?
    var sessionError:Error?
    private var certificationFile:LocalFile
    private var key:String
    
    required init(WithCertificationFilePath CertificationFile:LocalFile, WithKey Key:String)
    {
        certificationFile = CertificationFile
        key = Key
        super.init()
       
        self.createSessionManager(WithKeys: Key)
        
    }
    
    private func createSessionManager(WithKeys Key:String)
    {
        let adapter = MyRequestAdapter.init(accessToken: Key)
        let interceptor = Interceptor(adapters: [adapter])
        sessionManager = Session.init(configuration: URLSessionConfiguration.ephemeral, delegate: self,interceptor:interceptor )
    }
    
  
    override func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    {
        let serverTrust = challenge.protectionSpace.serverTrust
        let certificate = SecTrustGetCertificateAtIndex(serverTrust!, 0)
    
        // Set SSL policies for domain name check
        let policies = NSMutableArray();
        policies.add(SecPolicyCreateSSL(true, (challenge.protectionSpace.host as CFString)))
        SecTrustSetPolicies(serverTrust!, policies);
        
        // Evaluate server certificate
        let isServerTrusted = SecTrustEvaluateWithError(serverTrust!, nil )
            
        // Get local and remote cert data
        let remoteCertificateData:NSData = SecCertificateCopyData(certificate!)
    
        let FileName = self.certificationFile.completeFilePathAndName
        do
        {
            if  let Content = try self.certificationFile.fetchContent(),
                let CerFileContent = Content as? Data
            {
                
                if (isServerTrusted && remoteCertificateData.isEqual(to: CerFileContent ))
                {
                    let credential:URLCredential = URLCredential(trust: serverTrust!)
                    completionHandler(.useCredential, credential)
                }
                else
                {
                    completionHandler(.cancelAuthenticationChallenge, nil)
                    
                    self.sessionError = DataFetchedError.SSLPinnigError(CertificationFileName: FileName, ErrorDescription: "Certification file not valid for connection to domain")
                }
                
            }
            else
            {
                completionHandler(.cancelAuthenticationChallenge, nil)
                self.sessionError = DataFetchedError.SSLPinnigError(CertificationFileName: FileName, ErrorDescription: "Certification file content is not valid")
            }
        }
        catch
        {
            completionHandler(.cancelAuthenticationChallenge, nil)
            self.sessionError = error
        }
    }

}


class MyRequestAdapter: RequestAdapter
{
    private let accessToken: String

    init(accessToken: String)
    {
        self.accessToken = accessToken
    }
    
    // function to add header for every request in session
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void)
    {
        var adaptedRequest = urlRequest // because `urlRequest` is a parameter and parameters are not mutable
    
        adaptedRequest.headers.update(.authorization(accessToken))
        completion(.success(adaptedRequest))
        
    }
}
