//
//  LocalFileTest.swift
//  AwsomeDataFetcherTests
//
//  Created by Didier Lobeau on 27/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import XCTest

class LocalFileTest: XCTestCase
{

    var localFile:LocalFile?
     var testEnvironment:TestEnvironment?
    
    override func setUp()
    {
        testEnvironment = TestEnvironment.init()
        
        let CurrenBundle = testEnvironment!.bundle
        
        localFile = LocalFileImp.init(WithBundle: CurrenBundle, WithURL:"FileForTest.crt")
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLocalFile_whenFileContentUpdate_changesMustBeSerialized()
    {
        do
        {
            if(localFile!.isFileExist())
            {
                try localFile?.cleanFileFromUserEnvironment()
            }
            XCTAssertTrue(!localFile!.isFileExist())
            
            let ContentForFile = "un chasseur sachant chasser sans son chien est un bon chasseur"
            let DataToBeAdd =  ContentForFile.data(using: .utf8)
            localFile?.update(WithContent:DataToBeAdd!)
            
            XCTAssertTrue(localFile!.isFileExist())
            
            let UnderTest = try localFile?.fetchContent() as? Data
            
            let StringUnderTest = String.init(data: UnderTest!, encoding: .utf8)
            
            XCTAssertEqual(ContentForFile, StringUnderTest)
            
        }
        catch
        {
            XCTAssertTrue(false, error.localizedDescription)
        }
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    

}
