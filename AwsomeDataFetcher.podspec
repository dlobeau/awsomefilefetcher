
Pod::Spec.new do |spec|

  spec.name         = "AwsomeDataFetcher"
  spec.version      = "1.4.0"
  spec.summary      = "A CocoaPods library to fetch generic data"

  spec.description  = "A CocoaPods library written in Swift to fetch generic data over the network"

  spec.homepage     = "https://gitlab.com/dlobeau/awsomefilefetcher"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "didier lobeau" => "didier.lobeau@gmail.com" }

  spec.ios.deployment_target = "12.1"
  spec.swift_version = "4.2"

  spec.source        = { :git => "https://gitlab.com/dlobeau/awsomefilefetcher.git", :tag => "#{spec.version}" }
  spec.source_files  = "AwsomeDataFetcher/**/*.{h,m,swift}"
	
	spec.dependency 'Alamofire'
end

