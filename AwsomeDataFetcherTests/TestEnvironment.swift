//
//  TestEnvironment.swift
//  AwsomeDataFetcherTests
//
//  Created by Didier Lobeau on 11/02/2020.
//  Copyright © 2020 Didier Lobeau. All rights reserved.
//

import Foundation
import XCTest

class TestEnvironment
{
    func createFetcherDelegate(WithExpectation Expectation:XCTestExpectation)->(TestFetcherDelegate?)
    {
        return TestFetcherDelegate.init(WithExpectation: Expectation)
    }
    
    lazy var bundle:Bundle =
    {
        return Bundle(for: type(of: self))
    }()
    
}


class TestFetcherDelegate:DataFetcherDelegate
{
    func identification()->( String)
    {
        return self.fetchingExpectation.description
    }
    
    let fetchingExpectation:XCTestExpectation
    var fetchedData:Any?
    var errorReported:Error?
    func didFetchData(WithFetchedData DataFetched: Any?, WithError ErrorFetched:Error?)
    {
        self.fetchingExpectation.fulfill()
        self.fetchedData = DataFetched
        self.errorReported = ErrorFetched
    }
    init(WithExpectation Expectation:XCTestExpectation)
    {
        self.fetchingExpectation = Expectation
        
    }
    
}
